%% Lecture du fichier CSV station m�t�o GreEnER 2019

clear all
close all 

formatSpec = '%s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f';
fid = fopen('Meteo_GreEnER_2_01_Hour_2019.csv','r'); % ouverture du fichier

data = textscan(fid, formatSpec,'headerlines', 4, 'delimiter',','); % lecture du fichier
fclose(fid); % fermeture du fichier


DATES = data{1,1} ;
RECORD = data{1,2} ;
CR6_Alim_Avg = data{1,3} ;
CR6_Temp_Avg = data{1,4} ;
AirTemp_Avg = data{1,5} ;
RH_Avg = data{1,6} ;
Press_Avg = data{1,7} ;
WSpd_Avg = data{1,8} ;
WDir_Avg = data{1,9} ;
WDir_Std = data{1,10} ;
WSpd_Max = data{1,11} ;
Rain_Tot = data{1,12} ;
Ray_Global_CMP3_Avg = data{1,13} ;
Ray_Global_CMP3_Energy_Tot = data{1,14} ;
Ray_Global_RSR2_Avg = data{1,15} ;
Ray_Direct_RSR2_Avg = data{1,16} ;
Ray_Direct_H_RSR2_Avg = data{1,17} ;
Ray_Diffus_RSR2_Avg = data{1,18} ;
Ray_Global_RSR2_Energy_Tot = data{1,19} ;
Ray_Direct_RSR2_Energy_Tot = data{1,20} ;
Ray_Diffus_RSR2_Energy_Tot = data{1,21} ;
Ray_Global_RSR2_Raw_Avg = data{1,23} ;
Ray_Direct_RSR2_Raw_Avg = data{1,23} ;
Ray_Diffus_RSR2_Raw_Avg = data{1,24} ;
ZenDeg_Avg = data{1,25} ;
WM_Spd_WV_Avg = data{1,26} ;
WM_Dir_Avg = data{1,27} ;
WM_Dir_Std = data{1,28} ;
WM_Spd_Avg = data{1,29} ;
WM_Spd_Std = data{1,30} ;
WM_Spd_Max = data{1,31} ;
WM_AV_Spd_Avg = data{1,32} ;
WM_AV_Spd_Std = data{1,33} ;
WM_AV_Spd_Max = data{1,34} ;
WM_vit_Son_Avg = data{1,35} ;
WM_Temp_Avg = data{1,36} ;


